# Bounded Confidence

Implementation of the bounded confidence model (opinion dynamics) available [online here](https://lisc.pages.mia.inra.fr/boundary-confidence).
